'''
    project name    : youtube download script 
    by              : tkdev
    objective       : 
    last update     : 16 aug 22
    log             :
                    : 16 aug 22
                    : - bug fix -> YoutubeDL error 403 by remove cache
                    :
                    : 21 feb 22    
                    : + direct download for only one file 
                    : + batch loader
                    : + 2 mode (fast and live stream) download 
                    : - bug fix 'content-lenght error'
                    : - bug fix -> update pytube v12+
                    : - bug fix -> directory not found -> create new dir

    remark          :
        - save url youtube list into "links_file.txt"  for batch download
        - header paramter
            1 rem= comment
            2 path= target save folder
            3 URL list for download 

        - error details please see in  -> error_log.txt
'''

from pytube import YouTube
from youtube_dl import YoutubeDL
from time import sleep
import string
import platform
import os
import re

def save_log(log:str, path:str, url:str ):
    the_file = open('error_log.txt', 'a', encoding="utf8" )
    the_file.write("rem=" + log + "\n")
    the_file.write("path=" + path + "\n")
    the_file.write( url + "\n\n")
    the_file.close()


# live stream  download for fix "content-lenght error"
def live_youtube_dl(url: str, save_target: str, index: str = '1'):
    print("#### Switch to old method for : " + url)

    ydl_opts = {'outtmpl': save_target}
    try:
        ytdownload = YoutubeDL(ydl_opts)
        ytdownload.cache.remove()        
        ytdownload.download([url])        
        print("#### old method : download completed! ")
        #
        # remove special charactoer
        # and move downloaded file to correct file path
        info = ytdownload.extract_info(url, download=False)
        chars = re.escape(string.punctuation)
        filename = re.sub(r'['+chars+']', '', info['title']) + ".mp4"
        os.replace(save_target, os.path.dirname( save_target ) + "\\" + index + filename )
        print("#### old method : rename file completed!\n")        
    except Exception as e:
        print("#### Old method still Error: " + url)
        print(str(e))
        save_log("#### Old method still error : " + str(e), os.path.dirname( save_target ), url)

def youtube_dl(url: str, save_target: str, index: str = ""):
    # get youtube title
    ytdownload = YoutubeDL()
    info = ytdownload.extract_info( url, download=False)
    chars = re.escape(string.punctuation)
    title = re.sub(r'['+chars+']', '', info['title']) + ".mp4"
    print ("...save location = " + save_target + "\\" + index + title)

    #YouTube(line).streams.filter(res="1080p").first().download( save_target )
    YouTube(url).streams.filter(res="1080p").first().download(output_path = save_target, filename = index + title)

# create new folder if not exists
def create_new_folder(folder_name:str):
    # tips1
    #if not os.path.exists( SAVE_PATH ):
    #    os.makedirs( SAVE_PATH )

    # tips2
    # os.makedirs(SAVE_PATH) if not os.path.exists( SAVE_PATH ) else ""

    # tips3 very cool !!
    [lambda:"", lambda:os.makedirs(SAVE_PATH)] [not os.path.exists( SAVE_PATH )] ()


def batch_download( links_file: str ):
    if not os.path.isfile( links_file ):
        print("file " +links_file+  " not found")
        return
    
    # link of the video to be downloaded
    links = open( links_file, 'r', encoding="utf8") 
    print("batch download starting...")
    counter_id = 0
    index = ""
    for line in links:
        counter_id += 1
        # remove '&list' parameter from URL (if not the library will download all file in list)
        line = line.strip()
        start_list_index = line.find("&list=")
        stop_list_index = line.find("&", start_list_index+6) if start_list_index > -1 else -1
        line = line[:start_list_index] + line[stop_list_index:] if start_list_index > -1 else line

        if(len(line) > 2):
            if(line.startswith("rem")):
                # print("this line is rem : " + line.split('=')[1].strip())
                pass
            elif(line.startswith("path")):
                SAVE_PATH = line.split('=')[1].strip()
                create_new_folder(SAVE_PATH)
                print("current save folder : " + SAVE_PATH)            
            else:            
                try:
                    # download youtube
                    # find index for set filename
                    find_index = line.find("index=")
                    index =  line[find_index+6:].strip() + " " if find_index > -1 else ""

                    # start download
                    youtube_dl(line, index)

                    print("####" + line + "..downloaded")
                    print('sleep 5 second..\n')
                    sleep(5)
                except Exception as ex:
                    # to handle exception
                    print("Connection Error at " + line)
                    print(str(ex))
                    #save_log(str(ex), SAVE_PATH, line)
                    live_youtube_dl(line.split('&')[0], SAVE_PATH + "\\old-method-" + str(counter_id), index )


# where to save
SAVE_PATH = input("set default save folder >> ")
if len(SAVE_PATH) < 2:
    if platform.system().startswith("Windows"):
        SAVE_PATH = r'S:\__TmpYoutubeVDO' # default
    else:
        SAVE_PATH = r'~/__TmpYoutubeVDO'

create_new_folder(SAVE_PATH)

URL_Youtube = input("URL Youtube or [Enter] to load 'links_file.txt' : ")
if len(URL_Youtube) > 2:
    try:
        youtube_dl( URL_Youtube, SAVE_PATH )
    except:
        live_youtube_dl( URL_Youtube, SAVE_PATH + "\\old-method-" )
else:
    batch_download( 'links_file.txt' )

print('End.')

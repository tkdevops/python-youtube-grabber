# python-youtube-grabber
```
สคริป python ใช้โหลด vdo จากยูทูป มาเก็บไว้ดู โดยที่นี้ใช้ library หลักคือ youtube_dl กับ pytube
ในกรณี error จะมีการเลือกใช้ library แบบอัตโนมัติ 
```

# ติดตั้ง library
```
- pip install pytube
- pip install youtube_dl
```

# รายละเอียด
สามารถทำงานเป็น batch เพื่อโหลด vdo จำนวนมากแล้วทิ้งเครื่องไว้ได้ 
โดยสามารถดูได้จากไฟล์ links_file.txt ซึ่งคีย์เวิร์ดมีดังนี้
`rem` ใช้เป็นคอมเมนต์, `path` ใช้เป็นที่เก็บ ส่วนบรรทัดถัดไปจะเป็น URL ที่จะโหลด vdo มาเก็บ
<br />

```
ตัวอย่าง 
rem= ******* remark docements here !! **********
path=S:\__Example1__\Save\Path
https://www.youtube.com/watch?v=file1
```

`TKVR`
